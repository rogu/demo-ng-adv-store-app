import { Component } from '@angular/core';
import { MainComponent } from './core/components/main/main.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    standalone: true,
    imports: [MainComponent]
})
export class AppComponent {
  title = 'demo-ng-adv-store-app';
}
