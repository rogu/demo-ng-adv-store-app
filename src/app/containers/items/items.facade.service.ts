import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpState, HttpStoreConfig } from '../../shared/services/http-state-service';
import { HttpResponseModel } from '../../shared/utils/api';
import { CartFacade } from '../cart/services/cart.facade.service';
import { StateAction } from '../../shared/services/state';

export type ItemsActions = 'fetchItems' | 'buy';
export interface ItemModel {
  id?: number;
  category: string;
  imgSrc: string;
  price: number;
  title: string;
}
export type ItemsState = ItemModel[];

@HttpStoreConfig({ url: 'https://api.debugger.pl' })
@Injectable({ providedIn: 'root' })
export class ItemsFacade extends HttpState<ItemsState> {
  constructor(http: HttpClient, private cartStore: CartFacade) {
    super([], http);
  }
  override dispatch<T extends ItemsActions>({ payload, type }: StateAction<T, ItemModel>): void {
    switch (type) {
      case 'fetchItems':
        this.get<HttpResponseModel<ItemModel>>('items', null, (resp) => resp.data);
        break;
      case 'buy':
        this.cartStore.dispatch({ type: 'increase', payload: payload as ItemModel });
        break;
    }
  }
}
