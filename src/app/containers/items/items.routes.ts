import { Routes } from '@angular/router';
import { ItemDetailResolver } from './item-detail.resolver';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { ItemsGuard } from './items.guard';
import { ItemsComponent } from './items/items.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [ItemsGuard],
    component: ItemsComponent,
    children: [
      {
        path: ':id',
        component: ItemDetailComponent,
        resolve: { itemDetail: ItemDetailResolver },
      },
    ],
  },
];
