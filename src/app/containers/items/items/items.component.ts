import { AsyncPipe } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { MatIconAnchor } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { RouterLink, RouterOutlet } from '@angular/router';
import { DataGridComponent } from 'debugger-ng-ui-lib';
import { Observable } from 'rxjs';
import { ItemModel, ItemsFacade } from '../items.facade.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
  standalone: true,
  imports: [DataGridComponent, MatIconAnchor, MatIcon, RouterLink, RouterOutlet, AsyncPipe],
})
export class ItemsComponent implements OnInit {
  items$!: Observable<ItemModel[]>;
  private itemsFacade = inject(ItemsFacade);

  ngOnInit(): void {
    this.items$ = this.itemsFacade.state$;
  }

  buy(payload: ItemModel) {
    this.itemsFacade.dispatch({ type: 'buy', payload });
  }
}
