import { inject } from '@angular/core';
import { CanActivateFn, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { ItemsFacade } from './items.facade.service';

export const ItemsGuard: CanActivateFn = ():
  | Observable<boolean | UrlTree>
  | Promise<boolean | UrlTree>
  | boolean
  | UrlTree => {
  const itemsFacade = inject(ItemsFacade);
  return itemsFacade.state$.pipe(
    tap((val) => !val.length && itemsFacade.dispatch({ type: 'fetchItems' })),
    filter((val) => !!val),
    map((v) => !!v)
  );
};
