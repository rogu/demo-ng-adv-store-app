import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ItemModel, ItemsFacade } from './items.facade.service';

export const ItemDetailResolver: ResolveFn<Observable<ItemModel | undefined>> = (
  route: ActivatedRouteSnapshot,
  state,
  itemsFacade = inject(ItemsFacade)
) =>
  itemsFacade
    .select((state) => state.find((item) => item.id === route.params['id']))
    .pipe(filter((val) => !!val));
