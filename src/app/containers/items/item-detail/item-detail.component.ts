import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AsyncPipe, KeyValuePipe } from '@angular/common';
import { DataGridComponent } from 'debugger-ng-ui-lib';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css'],
  standalone: true,
  imports: [DataGridComponent, AsyncPipe, KeyValuePipe]
})
export class ItemDetailComponent implements OnInit {
  data$!: Observable<any>;
  private route = inject(ActivatedRoute);

  ngOnInit(): void {
    this.data$ = this.route.data.pipe(map((val: any) => val.itemDetail));
  }
}
