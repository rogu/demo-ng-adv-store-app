import { inject, Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map, skip, withLatestFrom } from 'rxjs/operators';
import { CoreFacade } from '../../../core/services/core.facade.service';
import { StateAction, State } from '../../../shared/services/state';
import { Utils } from '../../../shared/utils/utils';
import { CartStorageService } from './cart.api.ls.service';
import { ItemModel } from '../../items/items.facade.service';

export type CartActionTypes = 'set' | 'increase' | 'decrease' | 'remove';

export interface CartItemModel extends ItemModel {
  count: number;
}

@Injectable({ providedIn: 'root' })
export class CartFacade extends State<CartItemModel[]> {
  coreFacade = inject(CoreFacade);
  storage = inject(CartStorageService);

  constructor() {
    super([]);
    this.state$
      .pipe(skip(1))
      .subscribe((state) => {
        this.coreFacade.dispatch({ type: 'loading', payload: true });
        this.storage.update(state).then(() => this.coreFacade.dispatch({ type: 'loading', payload: false }));
      });
  }

  override dispatch<T extends CartActionTypes>({
    type,
    payload,
  }: Required<StateAction<T, ItemModel | CartItemModel[]>>) {
    of(payload)
      .pipe(
        withLatestFrom(this.state$),
        map(([data, state]: [ItemModel | CartItemModel[], CartItemModel[]]) => {
          switch (type) {
            case 'set':
              return data as CartItemModel[];
            case 'increase':
              return Utils.addOrIncreaseParam(state, data);
            case 'decrease':
              return Utils.removeOrDecreaseParam(state, data);
            case 'remove':
              return Utils.remove(state, data);
            default:
              return state;
          }
        })
      )
      .subscribe((state) => this.setState(state));
  }
}
