import { Component, inject } from '@angular/core';
import { MatBadge } from '@angular/material/badge';
import { MatIcon } from '@angular/material/icon';
import { Observable } from 'rxjs';
import { CartFacade } from '../../services/cart.facade.service';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-cart-icon',
  imports: [MatIcon, MatBadge, CommonModule, RouterModule],
  standalone: true,
  templateUrl: './cart-icon.component.html',
  styleUrl: './cart-icon.component.scss',
})
export class CartIconComponent {
  private cartFacade = inject(CartFacade);
  cartCount$: Observable<number> = this.cartFacade.select((state) =>
    state.reduce((acc, item) => acc + item.count, 0)
  );
}
