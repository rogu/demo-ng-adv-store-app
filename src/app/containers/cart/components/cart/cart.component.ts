import { AsyncPipe } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, inject } from '@angular/core';
import { MatBadge } from '@angular/material/badge';
import { MatIconAnchor, MatMiniFabButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { DataGridComponent } from 'debugger-ng-ui-lib';
import { Observable } from 'rxjs';
import { CartFacade, CartItemModel } from '../../services/cart.facade.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [DataGridComponent, MatIcon, MatBadge, MatMiniFabButton, MatIconAnchor, AsyncPipe],
})
export class CartComponent implements AfterViewInit {
  cartFacade = inject(CartFacade);
  cd = inject(ChangeDetectorRef);
  cart$!: Observable<CartItemModel[]>;

  ngAfterViewInit(): void {
    this.cart$ = this.cartFacade.state$;
    this.cd.detectChanges();
  }

  increase(payload: CartItemModel) {
    this.cartFacade.dispatch({ type: 'increase', payload });
  }

  decrease(payload: CartItemModel) {
    this.cartFacade.dispatch({ type: 'decrease', payload });
  }

  remove(payload: CartItemModel) {
    this.cartFacade.dispatch({ type: 'remove', payload });
  }
}
