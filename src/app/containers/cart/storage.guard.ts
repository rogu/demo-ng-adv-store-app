import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CartFacade } from './services/cart.facade.service';
import { Api, HttpResponseModel } from '../../shared/utils/api';
import { ItemModel } from '../items/items.facade.service';

export const StorageGuard: CanActivateFn = () => {
  const cartFacade = inject(CartFacade);
  const http = inject(HttpClient);
  return from(cartFacade.storage.get()).pipe(
    tap((data) =>
      data?.length
        ? cartFacade.dispatch({ type: 'set', payload: data || [] })
        : http
            .get<HttpResponseModel<ItemModel>>(Api.MANDATORY_END_POINT)
            .subscribe(({ data }) =>
              data.forEach((item) => cartFacade.dispatch({ type: 'increase', payload: item }))
            )
    ),
    map((_) => true)
  );
};
