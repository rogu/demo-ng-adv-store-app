import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { ResolveFn } from '@angular/router';
import { FieldConfig } from 'debugger-ng-ui-lib';
import { map } from 'rxjs/operators';
import { Api } from '../../shared/utils/api';
import { Observable } from 'rxjs';

export const RegisterResolver: ResolveFn<Observable<FieldConfig[]>> = (
  route, state,
  http = inject(HttpClient)
) =>
  http
    .get<{ data: FieldConfig[] }>(Api.DATA_FORM_CONFIG)
    .pipe(map((resp: { data: FieldConfig[] }) => resp.data));
