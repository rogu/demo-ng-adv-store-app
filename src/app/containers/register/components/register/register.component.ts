import { AsyncPipe, JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FieldConfig, FORM_BG_COLOR, FormEvents, FormGeneratorComponent, FormValue } from 'debugger-ng-ui-lib';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  standalone: true,
  imports: [
    FormGeneratorComponent,
    AsyncPipe,
    JsonPipe
  ],
  providers:[
    { provide: FORM_BG_COLOR, useValue: '#eaeaea80' }
  ]
})
export class RegisterComponent {
  private route = inject(ActivatedRoute);
  formConfig$: Observable<FieldConfig[]> = this.route.data.pipe(map(({ config }) => config));
  errors!: any[] | null | undefined;

  onAction({ type, payload, errors }: FormValue) {
    this.errors = errors;
    if (type === FormEvents.submit && !errors?.length) alert(JSON.stringify(payload, null, 4));
  }
}
