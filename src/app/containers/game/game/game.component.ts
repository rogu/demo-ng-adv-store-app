import { AsyncPipe, KeyValuePipe, NgStyle } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EmbeddedViewRef,
  TemplateRef,
  ViewContainerRef,
  inject,
  viewChild,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { MatDialog, MatDialogActions, MatDialogClose, MatDialogContent } from '@angular/material/dialog';
import { MatFormField, MatHint, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { Observable, fromEvent, merge, of, pipe, throwError, timer } from 'rxjs';
import { catchError, filter, groupBy, map, mergeMap, retry, scan, switchMap, take, throttleTime } from 'rxjs/operators';
import { ActiveDirective } from '../../../shared/directives/active.directive';
import { CoreFacade } from '../../../core/services/core.facade.service';
import { GameApiService, Message } from '../services/game.api.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  standalone: true,
  imports: [
    ActiveDirective,
    NgStyle,
    MatDialogContent,
    MatFormField,
    MatLabel,
    FormsModule,
    MatInput,
    MatHint,
    MatDialogActions,
    MatButton,
    MatDialogClose,
    AsyncPipe,
    KeyValuePipe,
  ],
})
export class GameComponent implements AfterViewInit {
  playerTpl = viewChild<TemplateRef<{ $implicit: Message }>>('playerTpl');
  areaEl = viewChild<ElementRef>('area');
  registerTpl = viewChild<TemplateRef<ElementRef>>('registerTpl');
  name!: string;
  players: Map<string, EmbeddedViewRef<{ $implicit: Message }>> = new Map();
  stats$!: Observable<{ [k: string]: boolean }>;
  gameApi = inject(GameApiService);
  container = inject(ViewContainerRef);
  coreFacade = inject(CoreFacade);
  dialog = inject(MatDialog);

  ngAfterViewInit(): void {
    this.coreFacade.dispatch({ type: 'loading', payload: true });
    this.gameApi.getUser().subscribe(({ warning }) => {
      this.coreFacade.dispatch({ type: 'loading', payload: false });
      warning ? this.register() : this.init();
    });

    merge(
      fromEvent<MouseEvent | TouchEvent>(this.areaEl()!.nativeElement, 'mousemove'),
      fromEvent<MouseEvent | TouchEvent>(this.areaEl()!.nativeElement, 'touchmove')
    )
      .pipe(throttleTime(30))
      .subscribe((e: MouseEvent | TouchEvent) => {
        let clientX, clientY;
        if (e instanceof TouchEvent) {
          clientX = e.touches[0].clientX;
          clientY = e.touches[0].clientY;
        } else {
          clientX = e.clientX;
          clientY = e.clientY;
        }
        this.gameApi.messanger.next({ clientX, clientY } as any);
      });
  }

  updatePlayer(msg: Message) {
    const playerExists = this.players.get(msg.username as string);
    const state = { $implicit: msg };
    playerExists
      ? (playerExists.context = state)
      : this.players.set(msg.username!, this.container.createEmbeddedView(this.playerTpl()!, state));
  }

  get statsPipe() {
    return pipe(
      filter((msg: Message) => msg.username !== 'gift'),
      groupBy(({ username }) => username),
      mergeMap((g$) =>
        g$.pipe(
          switchMap(({ username, type }) =>
            timer(0, 1000).pipe(
              take(2),
              map((val) => ({ username, active: !val, type }))
            )
          )
        )
      ),
      scan((acc: any, msg: any) => {
        const result = { ...acc, [msg.username as string]: msg.active };
        msg.type === 'remove' && delete result[msg.username];
        return result;
      }, {})
    );
  }

  init() {
    this.gameApi.messanger.subscribe(this.updatePlayer.bind(this));
    this.stats$ = this.gameApi.messanger.pipe(this.statsPipe);

    fromEvent<{ target: HTMLElement }>(document, 'click')
      .pipe(filter(({ target: { id } }) => id === 'gift'))
      .subscribe(() => this.gameApi.messanger.next({ type: 'hit' }));
  }

  register() {
    of('your game nick according to pattern /^[a-zA-Z]{3,6}$/')
      .pipe(
        switchMap((data) => {
          const dialog = this.dialog.open(this.registerTpl()!, {
            data,
            height: '200px',
            width: '400px',
          });
          return dialog.afterClosed();
        }),
        switchMap((username) => this.gameApi.register(username as string)),
        catchError((error) => {
          console.log('register error', JSON.stringify(error));
          return throwError(() => error);
        }),
        retry(1)
      )
      .subscribe(this.init.bind(this));
  }
}
