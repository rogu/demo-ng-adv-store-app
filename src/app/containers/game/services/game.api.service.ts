import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';
import { Api } from '../../../shared/utils/api';

export interface Message {
  username?: string;
  clientX?: number;
  clientY?: number;
  size?: number;
  action?: string;
  type?: string;
  active?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class GameApiService {
  ws!: Subject<any>;
  private http = inject(HttpClient);

  register(username: string): Observable<any> {
    return this.http.post(
      Api.GAME_REGISTER_USER,
      { username },
      { withCredentials: true }
    );
  }

  getUser(): Observable<any> {
    return this.http.get(Api.GAME_GET_USER, { withCredentials: true });
  }

  get messanger(): Subject<Message> {
    return this.ws ? this.ws : (this.ws = webSocket(Api.GAME_PLAY));
  }
}
