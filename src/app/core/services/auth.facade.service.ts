import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { HttpState, HttpStoreConfig } from '../../shared/services/http-state-service';
import { NotificationService } from '../../shared/services/notification.service';
import { HttpResponseModel } from '../../shared/utils/api';
import { StateAction } from '../../shared/services/state';

export type AuthTypes = 'is-logged' | 'login' | 'logout';
export interface AuthDataModel {
  username: string;
  password: string;
}

@HttpStoreConfig({ url: 'https://auth.debugger.pl' })
@Injectable({ providedIn: 'root' })
export class AuthFacade extends HttpState<boolean | undefined> {
  constructor(private router: Router, http: HttpClient, private notification: NotificationService) {
    super(undefined, http);
  }

  override dispatch<T extends AuthTypes>({ payload, type }: StateAction<T, AuthDataModel>): void {
    switch (type) {
      case 'is-logged':
        const authorization = localStorage.getItem('token') || '';
        if (authorization) {
          const headers = new HttpHeaders({ authorization });
          this.get<HttpResponseModel<any>>(type, headers, (resp) => !resp.warning);
        } else this.setState(false);
        break;
      case 'login':
        this.post(type, payload, () => true)
          .pipe(
            tap(({ data }: any) => {
              this.router.navigateByUrl('/items');
              localStorage.setItem('token', data.accessToken);
            })
          )
          .subscribe();
        break;
      case 'logout':
        this.setState(false);
        localStorage.removeItem('token');
        this.notification.showSuccess('you are logged out');
        this.router.navigateByUrl('/login');
        break;
    }
  }
}
