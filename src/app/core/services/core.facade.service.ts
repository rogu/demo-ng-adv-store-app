import { Injectable } from '@angular/core';
import { StateAction, State } from '../../shared/services/state';

type CoreState = {
  loading: boolean;
  darkMode: boolean;
};
type CoreAction = keyof CoreState;

const initState: CoreState = { loading: false, darkMode: false };

@Injectable({ providedIn: 'root' })
export class CoreFacade extends State<CoreState> {
  constructor() {
    super(initState);
  }

  override dispatch({ payload, type }: Required<StateAction<CoreAction, boolean>>) {
    super.updateState({ [type]: payload });
  }
}
