import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { filter, map, take, tap } from 'rxjs/operators';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthFacade } from './auth.facade.service';
import { CanActivateFn } from '@angular/router';

export const AuthGuard: CanActivateFn = (route, state) => {
  const authFacade = inject(AuthFacade);
  const notification = inject(NotificationService);
  const router = inject(Router);
  return authFacade.state$
    .pipe(
      tap((logged) => {
        if (logged === undefined) {
          authFacade.dispatch({ type: 'is-logged' })
        } else if (!logged) {
          router.createUrlTree(['login']);
          notification.showError('you shall not pass; try login!');
        }
      }),
      filter((val) => !!val),
      map(v => !!v)
    );
};
