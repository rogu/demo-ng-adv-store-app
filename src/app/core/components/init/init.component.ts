import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { MatAnchor } from '@angular/material/button';

@Component({
    selector: 'app-init',
    templateUrl: './init.component.html',
    styleUrls: ['./init.component.scss'],
    standalone: true,
    imports: [MatAnchor, RouterLink]
})
export class InitComponent implements OnInit {

  ngOnInit() { }

}
