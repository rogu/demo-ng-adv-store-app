import { AsyncPipe } from '@angular/common';
import { Component, inject, OnInit, WritableSignal } from '@angular/core';
import {
  FieldConfig,
  FieldGeneratorDirective,
  FieldTypes,
  FORM_BG_COLOR,
  FormEvents,
  FormGeneratorComponent,
} from 'debugger-ng-ui-lib';
import { Observable } from 'rxjs';
import { StateAction } from '../../../shared/services/state';
import { AuthFacade } from '../../services/auth.facade.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  standalone: true,
  imports: [FormGeneratorComponent, FieldGeneratorDirective, AsyncPipe],
})
export class LoginComponent implements OnInit {
  formConfig: FieldConfig[] = [
    {
      label: 'username',
      name: 'username',
      value: 'admin@localhost',
      type: FieldTypes.input,
      placeholder: 'your name',
      validators: [
        { name: 'required', message: 'pole wymagane' },
        { name: 'email', message: 'email nie poprawny' },
      ],
    },
    {
      label: 'password',
      name: 'password',
      value: 'Admin1',
      type: FieldTypes.password,
      placeholder: 'your password',
      validators: [{ name: 'required', message: 'pole wymagane' }],
    },
    {
      label: 'login',
      name: 'send',
      type: FieldTypes.button,
    },
  ];
  auth$!: Observable<boolean | undefined>;
  private authFacade = inject(AuthFacade);
  public bgColor: WritableSignal<string> = inject(FORM_BG_COLOR);

  ngOnInit(): void {
    this.auth$ = this.authFacade.state$;
  }

  onAction({ type, payload }: StateAction<FormEvents, any>) {
    switch (type) {
      case FormEvents.submit:
        this.authFacade.dispatch({ type: 'login', payload });
        break;
      case FormEvents.update:
        console.log(payload);
        break;
    }
  }
}
