import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { Component, inject, viewChild } from '@angular/core';
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { MatListItem, MatNavList } from '@angular/material/list';
import { MatSidenav, MatSidenavContainer, MatSidenavContent } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbar } from '@angular/material/toolbar';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import * as pack from '../../../../../package.json';
import { SpinnerComponent } from '../../../shared/components/spinner/spinner.component';
import { AuthFacade } from '../../services/auth.facade.service';
import { CoreFacade } from '../../services/core.facade.service';
import { CartIconComponent } from '../../../containers/cart/components/cart-icon/cart-icon.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  standalone: true,
  imports: [
    MatSidenavContainer,
    MatSidenav,
    MatToolbar,
    RouterLink,
    MatNavList,
    MatListItem,
    RouterLinkActive,
    MatSidenavContent,
    MatIconButton,
    MatIcon,
    MatButton,
    MatSlideToggleModule,
    RouterOutlet,
    SpinnerComponent,
    CommonModule,
    CartIconComponent,
  ],
})
export class MainComponent {
  version = pack.version;
  private breakpointObserver = inject(BreakpointObserver);
  private authFacade = inject(AuthFacade);
  private coreFacade = inject(CoreFacade);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );
  darkMode$ = this.coreFacade.state$.pipe(map(({ darkMode }) => darkMode));
  auth$: Observable<boolean | undefined> = this.authFacade.state$;
  drawer = viewChild<MatSidenav>('drawer');

  logOut() {
    this.authFacade.dispatch({ type: 'logout' });
  }

  toggleDark() {
    this.coreFacade.dispatch({ type: 'darkMode', payload: !this.coreFacade.value.darkMode });
  }

  navClickHandler() {
    this.isHandset$.pipe(first()).subscribe((mobile) => {
      if (mobile) {
        this.drawer()!.toggle();
      }
    });
  }
}
