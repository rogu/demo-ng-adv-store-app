import { Routes } from '@angular/router';
import { StorageGuard } from './containers/cart/storage.guard';
import { RegisterResolver } from './containers/register/register.resolver';
import { InitComponent } from './core/components/init/init.component';
import { LoginComponent } from './core/components/login/login.component';
import { AuthGuard } from './core/services/auth.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [StorageGuard],
    children: [
      { path: '', component: InitComponent },
      { path: 'login', component: LoginComponent },
      {
        path: 'register',
        resolve: { config: RegisterResolver },
        loadComponent: () =>
          import('./containers/register/components/register/register.component').then((c) => c.RegisterComponent),
      },
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: 'game',
            loadComponent: () => import('./containers/game/game/game.component').then((c) => c.GameComponent),
          },
          {
            path: 'items',
            loadChildren: () => import('./containers/items/items.routes').then((c) => c.routes),
          },
          {
            path: 'cart',
            loadComponent: () => import('./containers/cart/components/cart/cart.component').then((c) => c.CartComponent),
          },
        ],
      },
      { path: '**', component: InitComponent },
    ],
  },
];
