import { AsyncPipe } from '@angular/common';
import { Component, OnInit, WritableSignal, inject, signal } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { RouteConfigLoadStart, Router } from '@angular/router';
import { Observable, combineLatest, iif, of } from 'rxjs';
import { delay, map, switchMap } from 'rxjs/operators';
import { CoreFacade } from '../../../core/services/core.facade.service';

@Component({
  selector: 'app-spinner',
  template: `
    @if (show()) {
    <div class="wrapper">
      <div class="circle">
        <mat-spinner diameter="50" color="warn"></mat-spinner>
      </div>
    </div>
    }
  `,
  styles: [
    `
      .wrapper {
        position: absolute;
        width: 100%;
        height: 100vh;
        background-color: rgba(0, 0, 0, 0.6);
        left: 0;
        z-index: 100;
        top: 0;
        transition: background-color 200ms linear;
        display: flex;
      }
      .circle {
        margin: auto;
      }
    `,
  ],
  standalone: true,
  imports: [MatProgressSpinner, AsyncPipe],
})
export class SpinnerComponent implements OnInit {
  router = inject(Router);
  show: WritableSignal<boolean> = signal<boolean>(false);
  coreFacade = inject(CoreFacade);
  loading$: Observable<boolean> = this.coreFacade.state$.pipe(map((state) => state.loading));

  ngOnInit() {
    combineLatest([
      this.loading$, 
      this.router.events.pipe(map((ev) => ev instanceof RouteConfigLoadStart))
    ])
      .pipe(switchMap((ev: any[]) => 
        iif(() => ev.includes(true), of(true), of(false).pipe(delay(500)))))
      .subscribe((ev) => this.show.set(ev));
  }
}
