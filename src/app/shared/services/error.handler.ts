import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, inject, Injectable, NgZone } from '@angular/core';
import { NotificationService } from './notification.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  private zone = inject(NgZone);
  private notification = inject(NotificationService);

  handleError(error: any) {
    // Check if it's an error from an HTTP response
    if (!(error instanceof HttpErrorResponse)) {
      error = error.rejection; // get the error object
    }
    this.zone.run(() => {
      // alert(error?.message || 'any error')
    });
    this.notification.showError(error.message);
  }
}
