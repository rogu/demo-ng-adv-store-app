import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { CoreFacade } from '../../core/services/core.facade.service';
import { NotificationService } from './notification.service';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {
  coreFacade = inject(CoreFacade);
  notification = inject(NotificationService);

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.coreFacade.dispatch({ type: 'loading', payload: true });
    const reqClone = this.addToken(req, localStorage.getItem('token'));
    return next.handle(reqClone).pipe(
      tap((evt) => {
        if (evt instanceof HttpResponse) {
          this.coreFacade.dispatch({ type: 'loading', payload: false });
          switch (true) {
            case !!evt.body.success:
              this.notification.showSuccess(evt.body.success);
              break;
            case !!evt.body.info:
              this.notification.showInfo(evt.body.info);
              break;
            case !!evt.body.warning:
              this.notification.showWarning(evt.body.warning);
              break;
          }
        }
      }),
      catchError(({ error: { error }, status }) => {
        this.coreFacade.dispatch({ type: 'loading', payload: false });
        this.notification.showError(error);
        return throwError(() => error);
      })
    );
  }

  addToken(req: HttpRequest<any>, token: string | null) {
    const opt = token ? { setHeaders: { Authorization: token } } : {};
    return req.clone({ ...opt, withCredentials: true });
  }
}
