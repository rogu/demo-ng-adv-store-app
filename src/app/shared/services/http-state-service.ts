import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, share, tap } from 'rxjs/operators';
import { State } from './state';

export function HttpStoreConfig(config: { url: string }): ClassDecorator {
  return function (constructor: any) {
    constructor.prototype.url = config.url;
  };
}

@HttpStoreConfig({ url: 'http://localhost/' })
export abstract class HttpState<T> extends State<T> {
  url!: string;

  constructor(state: T, private http: HttpClient) {
    super(state);
  }
  /**
   * @param  {string} extendUrl
   * @param  {HttpHeaders|null} headers
   * @param  {(resp)=>any} mapFn; get from response what you want save in store
   */
  protected get<T>(extendUrl: string, headers: HttpHeaders | null = null, mapFn: (resp: T) => any) {
    const req$ = this.http.get<T>(`${this.url}/${extendUrl}`, { ...(headers && headers) }).pipe(share());
    req$
      .pipe(
        map(mapFn),
        tap((resp) => this.setState(resp))
      )
      .subscribe();
    return req$;
  }

  protected post(extendUrl: string, data: any, mapFn: (resp: any) => any) {
    const req$ = this.http.post(`${this.url}/${extendUrl}`, data).pipe(share());
    req$
      .pipe(
        map(mapFn),
        tap((resp) => this.setState(resp))
      )
      .subscribe();
    return req$;
  }
}
