import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideRouter } from '@angular/router';
import { DataGridModule, FORM_BG_COLOR } from 'debugger-ng-ui-lib';
import { AppComponent } from './app/app.component';
import { routes } from './app/app.routes';
import { XhrInterceptor } from './app/shared/services/xhr.interceptor';
import { environment } from './environments/environment';
import { CartIDBService } from './app/containers/cart/services/cart.api.idb.service';
import { CartStorageService } from './app/containers/cart/services/cart.api.ls.service';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(DataGridModule.forRoot({ bgColor: 'white' }),),
    provideRouter(routes),
    { provide: CartStorageService, useClass: CartIDBService },
    { provide: FORM_BG_COLOR, useValue: '#eaeaea80' },
    { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true },
    /* { provide: ErrorHandler, useClass: GlobalErrorHandler }, */
    provideAnimations(),
    provideHttpClient(withInterceptorsFromDi()),
    provideAnimationsAsync(),
    provideAnimationsAsync(),
  ],
}).catch((err) => console.error(err));
